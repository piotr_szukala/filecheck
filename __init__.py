# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FileCheck
                                 A QGIS plugin
 Sprawdza obecność plików
                             -------------------
        begin                : 2016-01-15
        copyright            : (C) 2016 by Piotr Szukała- LUCATEL
        email                : piotr.szukala@lucatel.pl
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load FileCheck class from file FileCheck.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .file_check import FileCheck
    return FileCheck(iface)
