#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FileCheck
                                 A QGIS plugin
 Sprawdza obecność plików
                              -------------------
        begin                : 2016-01-15
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Piotr Szukała- LUCATEL
        email                : piotr.szukala@lucatel.pl
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, pyqtSlot
from PyQt4.QtGui import QAction, QIcon, QFileDialog, QProgressBar
from qgis.core import *
from qgis.gui import QgsMessageBar
import qgis.utils
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import csv
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from file_check_dialog import FileCheckDialog
import os.path
import datetime


class FileCheck:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'FileCheck_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = FileCheckDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Raport- sprawdzanie plików')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'FileCheck')
        self.toolbar.setObjectName(u'FileCheck')

        #Dodaje funkjonalność otwarcia okna z wyborem miejsca zapisu pliku po kliknięciu wyboru lokalizacji
        self.dlg.lineEdit_raport.clear()
        self.dlg.pushButton_raport.clicked.connect(self.select_output_raport)

        self.dlg.lineEdit_foto.clear()
        self.dlg.pushButton_foto.clicked.connect(self.select_localization_foto)

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('FileCheck', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/FileCheck/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Raport- sprawdzanie plików'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Raport- sprawdzanie plików'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    # Otwiera okno z wyborem pliku gdzie ma być zapisany raport
    def select_output_raport(self):
        self.dlg.lineEdit_raport.clear()
        filename = QFileDialog.getSaveFileName(self.dlg, "Wybierz miejsce zapisu raportu ","", '*.csv')
        self.dlg.lineEdit_raport.setText(filename)

    # Otwiera okno wyboru folderu gdzie są liki do sprawdzenia
    def select_localization_foto(self):
        self.dlg.lineEdit_foto.clear()
        filedir = QFileDialog.getExistingDirectory(self.dlg, "Wybierz miejsce do sprawdzenia")
        self.dlg.lineEdit_foto.setText(filedir)


    def run(self):
        """Run method that performs all the real work"""
        # Obsługa menu wyboru wczytranej warstwy do projektu

        # self.dlg.comboBox_column.clear()
        # self.dlg.comboBox_layers.clear()
        # self.dlg.comboBox_layers.setCurrentIndex(0)

        layers = self.iface.legendInterface().layers()
        layer_list = []
        del layer_list[:]
        for layer in layers:
            layer_list.append(layer.name())
        self.dlg.comboBox_layers.clear()
        self.dlg.comboBox_layers.addItems(layer_list)

        selectedLayer = None
        selectedLayer = self.dlg.comboBox_layers

        #result_column = self.dlg.comboBox_column
        layer_filelds = None
        layer_filelds = self.dlg.comboBox_column


        #self.run.columns = ''
        # Dodaje listę column do comboBox_column dla wybranej wcześniej warstwy
        def columns_of_selected_layer(self):
            selectedLayerIndex = selectedLayer.currentIndex()
            columns = layers[selectedLayerIndex].pendingFields()
            column_list = []
            del column_list[:]
            for column in columns:
                column_list.append(column.name())
            layer_filelds.clear()
            layer_filelds.addItems(column_list)
            #print selectedLayerIndex

        selectedColumnIndex = self.dlg.comboBox_layers.currentIndex()


        # Obsługa menu wyboru kolumny dla zaznaczonej warstwy. Używa SIGNAL activated dla podświetlonego obiektu i odpala funkcję columns_of_selected_layer
        LayerActivated = self.dlg.comboBox_layers.activated.connect(columns_of_selected_layer)


        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if len(self.dlg.lineEdit_raport.text()) <= 0 or len(self.dlg.lineEdit_foto.text()) <= 0:
            qgis.utils.iface.messageBar().pushMessage("Error", u"Proszę uzupełnić pola wyboru miejsca zdjęć na dysku i miejsce zapisu raportu", level=qgis.gui.QgsMessageBar.INFO, duration=3)
        elif result:

            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            selectedLayerIndexResult = self.dlg.comboBox_layers.currentIndex()
            selectedLayerResult = layers[selectedLayerIndexResult]

            countFeatures = 0
            for f in selectedLayerResult.getFeatures():
                countFeatures = countFeatures + 1

            selectedColumnIndex = self.dlg.comboBox_column.currentIndex()
            fields = selectedLayerResult.pendingFields()
            fields_list = []
            for field in fields:
                fields_list.append(field)
                #print field
            selectedField = fields_list[selectedColumnIndex]

            foto_dir = str(self.dlg.lineEdit_foto.text())
            raport = self.dlg.lineEdit_raport.text()+'.csv'

            print "Wybrana warstwa to",selectedLayerResult.name()
            print "Wybrana kolumna to",selectedField.name()
            print "Sciezka do zdjec",foto_dir
            print "Miejsce zapisu raportu",raport

            # Progressbar pokazujący postęp
            messageProgress = qgis.utils.iface.messageBar().createMessage("Trwa przetwarzanie", u"Sprawdzanie plików")
            progress = QProgressBar()
            progress.setMinimum(0)
            progress.setMaximum(countFeatures)
            progress.setAlignment(Qt.AlignLeft|Qt.AlignVCenter)
            messageProgress.layout().addWidget(progress)
            qgis.utils.iface.messageBar().pushWidget(messageProgress, level=qgis.gui.QgsMessageBar.INFO, duration=5)

            '''Przygotowanie pliku csv do zapisu'''
            outputFile = open(raport, 'wb')
            outputWriter = csv.writer(outputFile)
            #outputWriter.writerow([('Raport sprawdzajacy czy wszystkie pliki z danej kolumny znajduja się w katalogu')])
            outputWriter.writerow(['Wybrana warstwa to: ', selectedLayerResult.name()])
            outputWriter.writerow(['Wybrana kolumna to: ', selectedField.name()])
            outputWriter.writerow(['Sciezka do zdjec: ', foto_dir])
            today = datetime.date.today()
            outputWriter.writerow(['Data: ', today])

            '''Pętla sprawdzająca obecność plików'''
            testList = []
            count = 0
            for f in selectedLayerResult.getFeatures():
                fileExistance = os.path.join(foto_dir, str(f[selectedColumnIndex])+'.jpg')
                fileExistance = os.path.isfile(fileExistance)
                fileName = str(f[selectedColumnIndex])+'.jpg'
                #print fileName, fileExistance
                count = count + 1
                testList.append(fileExistance)
                progress.setValue(count)

                if fileExistance == True:
                    outputWriter.writerow([fileName, 'OK'])
                else:
                    outputWriter.writerow([fileName, 'brak pliku'])

            outputFile.close()

            '''Komunikat końcowy informujący, czy raport przebiegł pomyślnie, czy pojawiły się jakieś braki'''
            #print testList
            if False in testList:
                qgis.utils.iface.messageBar().pushMessage(u"Brakuje jakiegoś pliku", u"Przejrzyj raport znajdujący się:"+str(raport), level=qgis.gui.QgsMessageBar.CRITICAL, duration=3)
            else:
                qgis.utils.iface.messageBar().pushMessage(u"Wszystko OK!", u"Możesz przejrzeć raport znajdujący się:"+str(raport), level=qgis.gui.QgsMessageBar.SUCCESS, duration=3)


